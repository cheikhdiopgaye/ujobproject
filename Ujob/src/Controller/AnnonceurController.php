<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Entity\Entreprise;
use App\Form\EntrepriseType;
use App\Form\UpdateUserType;
use App\Form\UpdateEntrepriseType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/** 
*  @Route("/api")
*/
class AnnonceurController extends FOSRestController {

    private $connecter;
    private $deconnecter;

    public function __construct()
    {
        $this -> connecter ="Connecter";
        $this -> deconnecter ="Deconnecter"; 
    }
  
/** 
* @Route("/inscriptionannonceur", name="inscriptionannonceur", methods={"POST"})
*/
public function addannonceur(Request $request, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder): Response
{

    
    $entreprise= new Entreprise();
    $form = $this->createForm(EntrepriseType::class, $entreprise);
    $data=$request->request->all();
    $form->submit($data);
    $entreprise->setLogo("dienne.png");
    $entityManager = $this->getDoctrine()->getManager();
    $entityManager->persist($entreprise);
    $entityManager->flush();

    $utilisateur = new User();
    $form=$this->createForm(UserType::class , $utilisateur);
    $form->handleRequest($request);
    $data=$request->request->all();
    $form->submit($data);

    $utilisateur->setRoles(["ROLE_ANNONCEUR"]);
    $utilisateur->setStatut($this->connecter);
    $utilisateur->setPassword($passwordEncoder->encodePassword($utilisateur,
    $form->get('password')->getData()
        )
        );
    $entityManager = $this->getDoctrine()->getManager();
    $utilisateur->setEntreprise($entreprise);
    $entityManager->persist($utilisateur);
    $entityManager->flush();
    return new Response('Un Annonceur est bien inscrit',Response::HTTP_CREATED); 
}


/** 
* @Route("/annonceur/bloquer/{id}", name="bloquer_debloquer_annonceur", methods={"GET"})
*/ 
    public function bloqueAnnonceur(EntityManagerInterface $manager,User $utilisateur=null)
    {
        if($utilisateur->getStatut() == $this->connecter){
            $utilisateur->setStatut($this->deconnecter);
            $texte= 'Annonceur déconnecter';
        }
        else{
            $utilisateur->setStatut($this->connecter);
            $texte='Annonceur connecter';
        }
        $manager->persist($utilisateur);
        $manager->flush();
        $afficher = [ $this->status => 200, $this->message => $texte];
        return $this->handleView($this->view($afficher,Response::HTTP_OK));
    }
    /** 
     * @Route("/annonceur/update/{id}", name="update_annonceur", methods={"POST"})
     */
    public function updateAnnonceur(User $annonceur ,Request $request, EntityManagerInterface $manager, ValidatorInterface $validator,UserPasswordEncoderInterface $encoder){
            
        if(!$annonceur){
            throw new HttpException(404,'Cet utilisateur n\'existe pas !');
        }
        $ancienPassword=$annonceur->getPassword();
        $form = $this->createForm(UpdateUserType::class,$annonceur);
        $data=json_decode($request->getContent(),true);//si json
        if(!$data){
            $data=$request->request->all();//si non json
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
        $form->submit($data);
        if(!$form->isSubmitted()){
            return $this->handleView($this->view($validator->validate($form)));
        }

        $annonceur->setPassword($ancienPassword);
        $annonceur->setStatut($annonceur->getStatut());
        $manager->persist($annonceur); 
        $manager->flush();
        $afficher = [
            $this->status => 200,
            $this->message => 'L\'utilisateur a été correctement modifié !'
        ];
        return $this->handleView($this->view($afficher,Response::HTTP_OK));
            
    }

    /**
     * @Route("/entreprise/update/{id}", name="update_entreprise", methods={"POST"})
     */
    public function updateEntreprise( Entreprise $entreprise ,Request $request, EntityManagerInterface $manager, ValidatorInterface $validator,UserPasswordEncoderInterface $encoder){
            
        $form = $this->createForm(UpdateEntrepriseType::class,$entreprise);
        $data=json_decode($request->getContent(),true);//si json
        if(!$data){
            $data=$request->request->all();//si non json
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
        $form->submit($data);
        if(!$form->isSubmitted()){
            return $this->handleView($this->view($validator->validate($form)));
        }

        if(!$entreprise->getLogo()){//s il ne change pas sa photo
            $entreprise->setLogo($entreprise->getLogo());
        }
        $manager->persist($entreprise); 
        $manager->flush();
        $afficher = [
            $this->status => 200,
            $this->message => 'L\'utilisateur a été correctement modifié !'
        ];
        return $this->handleView($this->view($afficher,Response::HTTP_OK));
            
    }
}